from django.shortcuts import render, redirect
from .models import Trabajador
from .forms import TrabajadorForm


def list_trabajadores(request):
    trabajadores = Trabajador.objects.all()
    return render(request, 'trabajadores.html', {'trabajadores': trabajadores})


def create_trabajador(request):
    form = TrabajadorForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('list_trabajadores')

    return render(request, 'trabajadores-form.html', {'form': form})


def update_trabajador(request, id):
    trabajador = Trabajador.objects.get(id=id)
    form = TrabajadorForm(request.POST or None, instance=trabajador)

    if form.is_valid():
        form.save()
        return redirect('list_trabajadores')

    return render(request, 'trabajadores-form.html', {'form': form, 'trabajador': trabajador})


def delete_trabajador(request, id):
    trabajador = Trabajador.objects.get(id=id)

    if request.method == 'POST':
        trabajador.delete()
        return redirect('list_trabajadores')

    return render(request, 'trabj-delete-confirm.html', {'trabajador': trabajador})
