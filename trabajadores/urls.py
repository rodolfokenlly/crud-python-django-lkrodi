from django.urls import path
from .views import list_trabajadores, create_trabajador, update_trabajador, delete_trabajador

urlpatterns = [
    path('', list_trabajadores, name='list_trabajadores'),
    path('new', create_trabajador, name='create_trabajador'),
    path('update/<int:id>/', update_trabajador, name='update_trabajador'),
    path('delete/<int:id>/', delete_trabajador, name='delete_trabajador'),
]


# CRUD - CREATE, READ, UPDATE, DELETE