from django.db import models


class Trabajador(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.CharField(max_length=8)
    fechaNacimiento = models.CharField(max_length=30)
    nacionalidad = models.CharField(max_length=50)

    def __str__(self):
        return self.nombres
